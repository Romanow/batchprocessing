package ru.romanow.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.*;
import org.springframework.batch.item.database.support.PostgresPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import ru.romanow.batch.model.Event;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by romanow on 14.09.16
 */
@Configuration
public class BatchProcessConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(BatchProcessConfiguration.class);

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
    private static final int PAGE_SIZE = 1000;
    private static final int CHUNK_SIZE = 1000;
    private final Map<String, Date> dayMap;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private DataSource dataSource;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private TaskExecutor executor;

    public BatchProcessConfiguration() {
        dayMap = new HashMap<String, Date>() {{
            put("2016.02.01", getDate("2016.08.15"));
            put("2016.02.02", getDate("2016.08.12"));
            put("2016.02.03", getDate("2016.09.09"));
            put("2016.02.04", getDate("2016.09.12"));
            put("2016.02.05", getDate("2016.09.11"));
            put("2016.02.06", getDate("2016.08.07"));
            put("2016.02.07", getDate("2016.09.04"));
            put("2016.02.08", getDate("2016.08.29"));
            put("2016.02.09", getDate("2016.08.26"));
            put("2016.02.11", getDate("2016.09.06"));
            put("2016.02.12", getDate("2016.08.11"));
            put("2016.02.13", getDate("2016.08.04"));
            put("2016.02.14", getDate("2016.08.05"));
            put("2016.02.15", getDate("2016.08.06"));
            put("2016.02.16", getDate("2016.08.31"));
            put("2016.02.17", getDate("2016.09.05"));
            put("2016.02.18", getDate("2016.08.25"));
            put("2016.02.19", getDate("2016.08.30"));
            put("2016.02.20", getDate("2016.08.09"));
            put("2016.02.21", getDate("2016.08.20"));
            put("2016.02.22", getDate("2016.09.03"));
            put("2016.02.23", getDate("2016.08.28"));
            put("2016.02.24", getDate("2016.08.22"));
            put("2016.02.25", getDate("2016.08.17"));
            put("2016.02.26", getDate("2016.08.14"));
            put("2016.02.27", getDate("2016.09.02"));
            put("2016.02.28", getDate("2016.08.16"));
            put("2016.02.29", getDate("2016.08.19"));
            put("2016.03.01", getDate("2016.08.02"));
            put("2016.03.02", getDate("2016.08.21"));
            put("2016.03.03", getDate("2016.09.08"));
            put("2016.03.04", getDate("2016.09.01"));
            put("2016.03.05", getDate("2016.08.23"));
            put("2016.03.06", getDate("2016.09.10"));
            put("2016.03.07", getDate("2016.08.24"));
            put("2016.03.08", getDate("2016.08.18"));
            put("2016.03.09", getDate("2016.09.07"));
            put("2016.03.10", getDate("2016.08.27"));
            put("2016.03.11", getDate("2016.08.10"));
            put("2016.03.12", getDate("2016.08.01"));
            put("2016.03.13", getDate("2016.08.13"));
            put("2016.03.14", getDate("2016.08.03"));
        }};
    }

    @Bean
    public Job batchUpdateJob() {
        return jobBuilderFactory
                .get("batchUpdate")
                .incrementer(new RunIdIncrementer())
                .listener(jobListener())
                .start(updateEventDateStep())
                .build();
    }

    @Bean
    public JobExecutionListener jobListener() {
        return new JobExecutionListener() {
            @Override
            public void beforeJob(JobExecution jobExecution) {
                logger.info("Starting execution job {}", jobExecution.getJobInstance().getJobName());
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
                logger.info("Finish execution job {} with status",
                            jobExecution.getJobInstance().getJobName(),
                            jobExecution.getStatus());
            }
        };
    }

    @Bean
    public Step updateEventDateStep() {
        return stepBuilderFactory.get("updateEventDateStep")
                .<Event, Event>chunk(CHUNK_SIZE)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter())
                .taskExecutor(executor)
                .listener(stepListener())
                .build();
    }

    @Bean
    public StepExecutionListener stepListener() {
        return new StepExecutionListener() {
            @Override
            public void beforeStep(StepExecution stepExecution) {
                logger.info("Starting executing step {}", stepExecution.getStepName());
            }

            @Override
            public ExitStatus afterStep(StepExecution stepExecution) {
                logger.info("Finish executing step {} with status {}. Read {} items, write {} items",
                            stepExecution.getStepName(),
                            stepExecution.getStatus(),
                            stepExecution.getReadCount(),
                            stepExecution.getWriteCount());
                return null;
            }
        };
    }

    @Bean
    public JdbcPagingItemReader<Event> itemReader() {
        JdbcPagingItemReader<Event> itemReader = new JdbcPagingItemReader<>();
        itemReader.setDataSource(dataSource);
        itemReader.setPageSize(PAGE_SIZE);
        itemReader.setQueryProvider(queryProvider());
        itemReader.setRowMapper((rs, rowNum) -> new Event()
                .setEventDate(rs.getDate("event_date"))
                .setUeid(UUID.fromString(rs.getString("ueid"))));

        return itemReader;
    }

    @Bean
    public PostgresPagingQueryProvider queryProvider() {
        PostgresPagingQueryProvider queryProvider = new PostgresPagingQueryProvider();
        queryProvider.setSelectClause("SELECT me.ueid as ueid, me.event_date as event_date, me.order_id as order_id");
        queryProvider.setFromClause("FROM sr_monitoring_event me ");
        queryProvider.setWhereClause(
                "WHERE (date_trunc('day', me.event_date) BETWEEN " +
                "       to_date('01-02-2016', 'dd-MM-yyyy') AND to_date('14-03-2016', 'dd-MM-yyyy')) AND " +
                "      date_trunc('day', me.created_date) = to_date('13-09-2016', 'dd-MM-yyyy') AND " +
                "      me.event_type_id in (3, 4, 8)");
        queryProvider.setSortKeys(new HashMap<String, Order>() {{
            put("order_id", Order.ASCENDING);
        }});
        return queryProvider;
    }

    @Bean
    public ItemProcessor<Event, Event> itemProcessor() {
        return item -> item.setEventDate(getDateFromMap(item.getEventDate()));
    }

    @Bean
    public JdbcBatchItemWriter<Event> itemWriter() {
        JdbcBatchItemWriter<Event> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("UPDATE sr_monitoring_event SET event_date = :eventDate WHERE ueid = :ueid");
        itemWriter.setItemSqlParameterSourceProvider(parameterSourceProvider());
        return itemWriter;
    }

    @Bean
    public ItemSqlParameterSourceProvider<Event> parameterSourceProvider() {
        return new BeanPropertyItemSqlParameterSourceProvider<>();
    }

    private Date getDate(String date) {
        try {
            return formatter.parse(date);
        } catch (ParseException exception) {
            throw new RuntimeException(exception);
        }
    }

    private Date getDateFromMap(Date eventDate) {
        return dayMap.get(formatter.format(eventDate));
    }
}
