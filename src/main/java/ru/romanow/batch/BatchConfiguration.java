package ru.romanow.batch;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Created by ronin on 13.09.16
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration
        extends DefaultBatchConfigurer {

    @Bean(destroyMethod = "destroy")
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    @Override
    protected JobLauncher createJobLauncher()
            throws Exception {
        final SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        final SyncTaskExecutor taskExecutor = new SyncTaskExecutor();
        simpleJobLauncher.setTaskExecutor(taskExecutor);
        simpleJobLauncher.setJobRepository(getJobRepository());
        simpleJobLauncher.afterPropertiesSet();
        return simpleJobLauncher;
    }
}
