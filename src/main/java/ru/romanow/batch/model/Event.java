package ru.romanow.batch.model;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ronin on 13.09.16
 */
public class Event {
    private UUID ueid;
    private Date eventDate;

    public UUID getUeid() {
        return ueid;
    }

    public Event setUeid(UUID ueid) {
        this.ueid = ueid;
        return this;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public Event setEventDate(Date eventDate) {
        this.eventDate = eventDate;
        return this;
    }
}
